class commonserver
{
	# Puppet manifest for my PHP dev machine
	
	class { 'yum':
		extrarepo => [ 'epel' , 'remi' ],
	}

	# Edit local /etc/hosts files to resolve some hostnames used on your application.
	host { 'localhost':
		ensure => 'present',
		target => '/etc/hosts',
		ip => '127.0.0.1',
		host_aliases => ['mysql']
	}
	
	# create a directory
	file { "/www":
		ensure => "directory",
	}

	# Miscellaneous packages.
	$misc_packages = ['vim-enhanced','telnet','zip','unzip','git','screen','libssh2','libssh2-devel', 'gcc', 'gcc-c++', 'autoconf', 'automake']
	package { $misc_packages: ensure => latest }

	class { 'apache': }
	
	firewall { '100 allow http and https access':
		port   => [80, 443],
		proto  => tcp,
		action => accept,
	}

	apache::vhost { 'centos.local':
		  port          => '80',
		  docroot       => '/var/www',
		  docroot_owner => 'vagrant',
		  docroot_group => 'vagrant',
	}

	# PHP useful packages. Pending TO-DO: Personalize some modules and php.ini directy on Puppet recipe.
	php::ini {
		'/etc/php.ini':
			display_errors	=> 'On',
			short_open_tag	=> 'On',
			memory_limit	=> '512M',
			date_timezone	=> 'Europe/Madrid',
			html_errors		=> 'On',
			require => Yumrepo["remi"],
	}
	include php::cli
	include php::mod_php5
	php::module { [ 'devel', 'pear', 'mysql', 'mbstring', 'xml', 'gd', 'tidy', 'pecl-apc', 'pecl-memcache', 'soap', 'pecl-xdebug' ]: }
	
	# MySQL packages and some configuration to automatically create a new database.
	class { '::mysql::server': }

	$additional_mysql_packages = [ "mysql-devel", "mysql-libs" ]
	package { $additional_mysql_packages: ensure => present }
}